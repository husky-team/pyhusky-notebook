import bindings.frontend as ph
import re


def is_valid_date(string):
    date = re.compile('\d?\d?/\d?\d?/\d\d\d\d$')
    if date.match(string) is not None:
        return True
    else:
        return False


ph.env.pyhusky_start(params={'disable_progress': True})


data = ph.env.load('/datasets/mernis/turkey.txt') \
        .map(lambda line: line.split()) \
        .map(lambda citizen: [citizen[index] for index in
             [i for i, s in enumerate(citizen) if is_valid_date(s)]]) \
        .flat_map(lambda dob: dob).map(lambda dob: (int(dob[-4:]), 1)) \
        .filter(lambda pair: pair[0] >= 1910 and pair[0] <= 1990) \
        .count_by_key().collect()
