import bindings.frontend as ph
import re

def is_valid_date(string):
    date = re.compile('\d?\d?/\d?\d?/\d\d\d\d$')
    if date.match(string) is not None:
        return True
    else:
        return False

ph.env.pyhusky_start(params={'disable_progress': True})


data = ph.env.load('/datasets/mernis/turkey.txt').map(lambda line: line.split()).cache() 

top_3_male_names_alltime = data \
        .filter(lambda citizen: 'E' in citizen) \
        .map(lambda citizen: (citizen[2], 1)) \
        .count_by_key() \
        .topk(3, (lambda name: name[1]), True)
top_3_male_names_alltime = [i[0] for i in top_3_male_names_alltime]

top_3_female_names_alltime = data \
        .filter(lambda citizen: 'K' in citizen) \
        .map(lambda citizen: (citizen[2], 1)) \
        .count_by_key() \
        .topk(3, (lambda name: name[1]), True)
top_3_female_names_alltime = [i[0] for i in top_3_female_names_alltime]


top_3_male_names_recently = data \
        .filter(lambda citizen: 'E' in citizen) \
        .map(lambda citizen: [citizen, [citizen[index] for index in [i for i, s in enumerate(citizen) if is_valid_date(s)]]]) \
        .filter(lambda citizen: len(citizen[1]) == 1) \
        .map(lambda citizen: [citizen[0][2], int(citizen[1][0][-4:])]) \
        .filter(lambda citizen: citizen[1] == 1990) \
        .map(lambda citizen: (citizen[0], 1)) \
        .count_by_key() \
        .topk(3, (lambda name: name[1]), True)
top_3_male_names_recently = [i[0] for i in top_3_male_names_recently]

top_3_female_names_recently = data \
        .filter(lambda citizen: 'K' in citizen) \
        .map(lambda citizen: [citizen, [citizen[index] for index in [i for i, s in enumerate(citizen) if is_valid_date(s)]]]) \
        .filter(lambda citizen: len(citizen[1]) == 1) \
        .map(lambda citizen: [citizen[0][2], int(citizen[1][0][-4:])]) \
        .filter(lambda citizen: citizen[1] == 1990) \
        .map(lambda citizen: (citizen[0], 1)) \
        .count_by_key() \
        .topk(3, (lambda name: name[1]), True)
top_3_female_names_recently = [i[0] for i in top_3_female_names_recently]


top_alltime_male_names_growth = data \
        .filter(lambda citizen: citizen[2] in top_3_male_names_alltime) \
        .map(lambda citizen: [citizen[2], [citizen[index] for index in [i for i, s in enumerate(citizen) if is_valid_date(s)]]]) \
        .filter(lambda citizen: len(citizen[1]) == 1) \
        .map(lambda citizen: [[citizen[0], int(citizen[1][0][-4:])],1]) \
        .count_by_key() \
        .collect()

top_recent_male_names_growth = data \
        .filter(lambda citizen: citizen[2] in top_3_male_names_recently) \
        .map(lambda citizen: [citizen[2], [citizen[index] for index in [i for i, s in enumerate(citizen) if is_valid_date(s)]]]) \
        .filter(lambda citizen: len(citizen[1]) == 1) \
        .map(lambda citizen: [[citizen[0], int(citizen[1][0][-4:])],1]) \
        .count_by_key() \
        .collect()

top_alltime_female_names_growth = data \
        .filter(lambda citizen: citizen[2] in top_3_female_names_alltime) \
        .map(lambda citizen: [citizen[2], [citizen[index] for index in [i for i, s in enumerate(citizen) if is_valid_date(s)]]]) \
        .filter(lambda citizen: len(citizen[1]) == 1) \
        .map(lambda citizen: [[citizen[0], int(citizen[1][0][-4:])],1]) \
        .count_by_key() \
        .collect()

top_recent_female_names_growth = data \
        .filter(lambda citizen: citizen[2] in top_3_female_names_recently) \
        .map(lambda citizen: [citizen[2], [citizen[index] for index in [i for i, s in enumerate(citizen) if is_valid_date(s)]]]) \
        .filter(lambda citizen: len(citizen[1]) == 1) \
        .map(lambda citizen: [[citizen[0], int(citizen[1][0][-4:])],1]) \
        .count_by_key() \
        .collect()

data.uncache()
